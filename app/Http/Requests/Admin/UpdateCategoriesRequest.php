<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'basecode' => 'max:2147483647|nullable|numeric',
            'url' => 'required|unique:categories,url',
            'mindeliveryordersum' => 'numeric',
            'deliverysum' => 'numeric',
            'deliverytime' => 'max:2147483647|nullable|numeric',
            'opentime' => 'nullable|date_format:H:i:s',
            'closetime' => 'nullable|date_format:H:i:s',
            'logo' => 'nullable|mimes:png,jpg,jpeg,gif',
        ];
    }
}
