<?php

namespace App\Http\Controllers\Api\V1;

use App\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePricesRequest;
use App\Http\Requests\Admin\UpdatePricesRequest;

class PricesController extends Controller
{
    public function index()
    {
        return Price::all();
    }

    public function show($id)
    {
        return Price::findOrFail($id);
    }

    public function update(UpdatePricesRequest $request, $id)
    {
        $price = Price::findOrFail($id);
        $price->update($request->all());
        

        return $price;
    }

    public function store(StorePricesRequest $request)
    {
        $price = Price::create($request->all());
        

        return $price;
    }

    public function destroy($id)
    {
        $price = Price::findOrFail($id);
        $price->delete();
        return '';
    }
}
