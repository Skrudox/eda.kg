<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PagesRequest;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductsRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;

class PagesController extends Controller
{
    //use FileUploadTrait;

    /**
     * Display a listing of Product.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('page_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('page_delete')) {
                return abort(401);
            }
            $pages = Page::onlyTrashed()->get();
        } else {
            $pages = Page::all();
        }

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating new Product.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('page_create')) {
            return abort(401);
        }


        return view('admin.pages.create');
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param  \App\Http\Requests\StoreProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PagesRequest $request)
    {
        if (! Gate::allows('page_create')) {
            return abort(401);
        }
        $page = Page::create($request->all());

        return redirect()->route('admin.pages.index');
    }


    /**
     * Show the form for editing Product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('page_edit')) {
            return abort(401);
        }

        //  $parentguids = \App\Category::get()->pluck('guid', 'id')->prepend(trans('quickadmin.qa_please_select'), '');

        $page = Page::findOrFail($id);

        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update Product in storage.
     *
     * @param  \App\Http\Requests\UpdateProductsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PagesRequest $request, $id)
    {
        if (! Gate::allows('page_edit')) {
            return abort(401);
        }
        //$request = $this->saveFiles($request);
        $page = Page::findOrFail($id);
        $page->update($request->all());



        return redirect()->route('admin.pages.index');
    }


    /**
     * Display Product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('page_view')) {
            return abort(401);
        }
        $page = Page::findOrFail($id);

        return view('admin.pages.show', compact('page'));
    }


    /**
     * Remove Product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('page_delete')) {
            return abort(401);
        }
        $page = Page::findOrFail($id);
        $page->delete();

        return redirect()->route('admin.pages.index');
    }

    /**
     * Delete all selected Product at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('page_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Page::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('page_delete')) {
            return abort(401);
        }
        $page = Page::onlyTrashed()->findOrFail($id);
        $page->restore();

        return redirect()->route('admin.pages.index');
    }

    /**
     * Permanently delete Product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('page_delete')) {
            return abort(401);
        }
        $page = Page::onlyTrashed()->findOrFail($id);
        $page->forceDelete();

        return redirect()->route('admin.pages.index');
    }
}
