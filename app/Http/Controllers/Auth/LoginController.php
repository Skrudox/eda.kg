<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        $url = \Redirect::intended()->getTargetUrl();
        $url = urldecode($url);
        $url = preg_replace('/\[\d\]/', '[]', $url);


        if ( $user->role_id != 1 ) {// do your margic here
            return redirect()->to(urldecode($url));
        }
        return redirect('/admin/users');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = url()->previous();

        $this->middleware('guest', ['except' => 'logout']);
    }

    
}
