<?php

namespace App\Http\Controllers;

use App\CategoryType;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        //echo \Hash::make('password');
        $pages = \App\Page::all();
        $foodTypes = \App\CategoryType::where('type', 'food')->groupBy('name')->get();
        $kitchenTypes = \App\CategoryType::where('type', 'kitchen')->groupBy('name')->get();
        \View::share('foodTypes', $foodTypes);
        \View::share('kitchenTypes', $kitchenTypes);
        \View::share('pages', $pages);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {

        $data = [];

        $data['is_ajax'] = 0;
        $data['is_leftbar'] = 0;
       // \Session::flash('message', 'test');
        return view('site.main', $data);


    }

    public function page(Request $request) {

        $url = $request->route('page');
        $page = \App\Page::where('url', $url)->first();
        $data['page'] = $page;

        return view('site.page', compact('page'));

    }

    public function search(Request $request)
    {
        $data = [];
        $products = \App\Product::where('isgroup', 0);//->get()->sortByDesc("id");


        $data['products'] = $products->get()->sortByDesc('id');
        if($request->ajax())
        {
            $data['is_ajax'] = 1;
        }
        else
        {
            $data['is_ajax'] = 0;
        }
        $data['is_leftbar'] = true;
        return view('home', $data);
    }

    public function category_view(Request $request) {

        $data = [];
        $url = $request->route('url');
        $data['category'] = \App\Category::where('url', $url)->first();
        if($request->route('sub'))
        {
            $comments = \App\CategoryReview::orderByDesc('id')->where('category_id', $data['category']->id)->get();
            $data['comments'] = \View::make('site.category_reviews', ['comments' => $comments]);
        }

        $data['is_ajax'] = 0;
        $data['working_days'] = $data['category']->generateWorkingDays();
        return view('site.category_view', $data);

    }
	
	public function categories(Request $request) {
		
		
		$s_type = $request->input('s_type');
		$query = $request->input('query');

		$data = [];
		$view = '';
		if(!$s_type && !$query)
		{
			//$list = \App\Product::all();
			$data['products'] = \App\Product::where('isgroup', 0)->get()->sortByDesc("id");
			$view = 'products';
		}
		else
		{
			if($s_type == 1)
			{
				$list = \App\Product::where('isgroup', 0)->where('name', 'LIKE', '%'.$query.'%')->get();
				$data['products'] = $list->sortByDesc("id");
				$view = 'products';
			}
			if($s_type == 2)
			{
				$list = \App\Product::where('isgroup', 1)->where('name', 'LIKE', '%'.$query.'%')->get();
				$data['products'] = $list->sortByDesc("id");
				$view = 'products';
			}
			if($s_type == 3)
			{
				$list = \App\Category::where('name', 'LIKE', '%'.$query.'%')->get();
				$data['categories'] = $list->sortByDesc("id");
				$view = 'categories';
			}
		}
		$data['is_ajax'] = false;
        return view('site.main_page_'.$view, $data);
	}
	
	public function categories_ajax(Request $request) {
		
		
		$s_type = $request->input('s_type');
		$query = $request->input('query');
		$data = [];
		$view = '';
		if(!$s_type && !$query)
		{
			$list = \App\Product::where('isgroup', 0)->get()->sortByDesc("id");
			$view = 'products';
		}
		else
		{
			if($s_type == 1)
			{
				$list = \App\Product::select('*', 'products.name', 'products.id', 'products.guid')->where('isgroup', 0)->where('products.name', 'LIKE', '%'.$query.'%');
				//$list = $list->join('prices', 'prices.guid', '=', 'products.guid');
                if($foodTypes = Input::get('food_type'))
                {
                    foreach($foodTypes as $key => $f)
                    {
                        $alias = 'f'.($key + 1);
                       // $alias2 = 'k'.($key + 1);
                        $list = $list->join('category_types AS '.$alias, 'products.parentguid_id', '=', $alias.'.guid');
                       // $list = $list->join('category_types AS '.$alias2, 'products.parentguid_id', '=', $alias2.'.guid');
                        if($key == 0)
                        {
                            $list = $list->where($alias.'.id', $f);
                           // $list = $list->orWhere($alias2.'.id', $f);
                        }
                        else
                        {
                            $list = $list->orWhere($alias.'.id', $f);
                           // $list = $list->orWhere($alias2.'.id', $f);
                        }
                    }
                }
				$data['products'] = $list->groupBy('products.id')->get();

				$view = 'products';
			}
			if($s_type == 2)
			{
				$list = \App\Product::where('isgroup', 1)->where('name', 'LIKE', '%'.$query.'%')->get();
				$data['products'] = $list->sortByDesc("id");
				$view = 'products';
			}
			if($s_type == 3)
			{
				$list = \App\Category::select('*', 'categories.name', 'categories.id', 'categories.guid')->where('categories.name', 'LIKE', '%'.$query.'%');
                if($foodTypes = Input::get('food_type'))
                {
                    $types = \App\CategoryType::whereIn('id', $foodTypes)->get();
                    foreach($foodTypes as $key => $f)
                    {

                        $alias = 'f'.($key + 1);
                        //$alias2 = 'k'.($key + 1);
                        $list = $list->join('category_types AS '.$alias, 'categories.guid', '=', $alias.'.guid');
                       // $list = $list->join('category_types AS '.$alias2, 'categories.guid', '=', $alias2.'.guid');

                        if($key == 0)
                        {
                            $list = $list->where($alias.'.name', $types[$key]->name);
                            //$list = $list->where($alias.'.guid', '=', 'categories.guid');
                          //  $list = $list->orWhere($alias2.'.id', $f);
                        }
                        else
                        {
                            $list = $list->orWhere($alias.'.name', $types[$key]->name);
                           // $list = $list->orWhere($alias2.'.id', $f);
                        }
                    }
                }
				$data['categories'] = $list->groupBy('categories.id')->get();
                //echo '<pre>';
                //print_r($data['categories']->count());
               // echo '</pre>';
                //exit;
                $cats = $data['categories'];

                foreach($cats as &$cat) {

                    $cat->working_days = $cat->generateWorkingDays();
                }
                $data['categories'] = $cats;
				$view = 'categories';
			}
		}
		if($request->input('firstSearch'))
        {
            $data['is_leftbar'] = false;
        }
        else
        {
            $data['is_leftbar'] = true;
        }
        $data['is_ajax'] = true;
        return view('home', $data);
	}
	
	public function cart_data() {
		
		
		$view = HomeController::cart_data2();
		return $view;
	}
	
	
	public static function calc_cart() {
		
		$data = [];
		$cart = \Session::get('cart') ? \Session::get('cart'): [];
		$data['products'] = array();
		$totalSum = 0;
		$place = '';
		foreach($cart as $p)
		{
			$product = \App\Product::find($p['id']);
			$price = 0;
			$media = '';
			$name = '';
			if($product)
			{
			    $name = $product->name;
			    if($product->price) {
                    $price = $product->price->price;
                }
				$media = $product->getFirstMediaUrl('photos');
			    if($product->parentguid)
                {
                    $place = $product->parentguid->name;
                }
			}
			$totalSum += $price;
			$data['products'][] = array('id' => $p['id'], 'img' => $media, 'count' => $p['count'], 'name' => $name, 'price' => $price, 'place' => $place);
			
		}
		$data['totalSum'] = $totalSum;
		
		return $data;
		
	}
	
	public static function cart_data2() {
		
		$data = HomeController::calc_cart();
		$view = \View::make('site.cart', $data);
		return $view;
	}
	
	public function cart_plus_item(Request $request) {
		
		$id = intval($request->input('id'));
		$data = \Session::get('cart') ? \Session::get('cart') : [];
		
			foreach($data as &$p)
			{
				if($p['id'] == $id)
				{
					$p['count']++;
					\Session::put('cart', $data );
					print_r($data);
					
					return;
				}
			}
	}
	public function cart_minus_item(Request $request) {
		
		$id = intval($request->input('id'));
		$data = \Session::get('cart') ? \Session::get('cart') : [];
		
			foreach($data as $key => &$p)
			{
				if($p['id'] == $id)
				{
					$p['count']--;
					if($p['count'] <= 0)
					{
						unset($data[$key]);
					}
					\Session::put('cart', $data );
					
					return;
				}
			}
	}

	public function popular_orders() {


        $data = [];
        $orders = \App\Order::select(\DB::raw('COUNT(*) AS count, id, product_ids'))->groupBy('product_ids')->get();
        foreach($orders as $key => &$o)
        {
            if($o->count < 3)
            {
                unset($orders[$key]);
                continue;
            }
            //$o->products = [];
            $ids = json_decode($o->product_ids);
            //sort($ids);
            $products = \App\Product::whereIn('id', $ids)->get();
            $o->products = $products;
        }
        $data['orders'] = $orders;
        $data['is_ajax'] = 0;
        $view = \View::make('site.popular_orders', $data);
        return $view;



    }

    public function popular_orders_get(Request $request) {

        $id = intval($request->route('id'));

        $order = \App\Order::find($id);
        $ids = json_decode($order->product_ids);
        foreach($ids as $id)
        {

            HomeController::add_to_cart2($id);
        }
        \Session::put('message', 'Набор добавлен в корзину');
        return \Redirect::back();


    }

	public function submit_cart(Request $request) {

        $data = HomeController::calc_cart();
       // print_r($data);
        $input = $request->input();
        //print_r($input);
       // exit;
        $str = '';
        $totalSum = 0;
        $ids = [];
        foreach($data['products'] as $p)
        {
            $ids[] = $p['id'];
            $sum = (floatval($p['price']) * intval($p['count']));
            $totalSum += $sum;

            // $sum;
          //  exit;

            $str .= '<div>'.$p['name'].' x'.$p['count'].' = <strong>'.$sum.' сом </strong>('.$p['place'].')</div>';

        }
        sort($ids);
        \App\Product::whereIn('id', $ids)->increment('ordered_times');
        $str .= '<p>Итого: <strong>'.$totalSum.' сом</strong></p>';
        $input['status'] = 0;
        $input['data'] = $str;
        $order = new \App\Order();
        $order->fill($input);
        if($user = \Auth::user())
        {
            $order->user_id = $user->id;
        }

        $order->product_ids = json_encode($ids);
        $order->save();
        \Session::put('cart', [] );
        \Session::flash('message', 'Ваш заказ принят');
        return \Redirect::back();


    }
	
	public function checkout_cart() {
		
		
		$data = HomeController::calc_cart();
		$view = \View::make('site.checkout', $data);
		return $view;
	}
	
	public function cart_delete_item(Request $request) {
		
		$id = intval($request->input('id'));
		$data = \Session::get('cart') ? \Session::get('cart') : [];
		
			foreach($data as $key => &$p)
			{
				if($p['id'] == $id)
				{
					unset($data[$key]);
					\Session::put('cart', $data );
					//print_r($data);
					
					return;
				}
			}
	}
	public function add_to_cart(Request $request) {
		
		$id = intval($request->input('id'));
		$data = \Session::get('cart') ? \Session::get('cart') : [];
		
			foreach($data as &$p)
			{
				if($p['id'] == $id)
				{
					$p['count']++;
					\Session::put('cart', $data );
					//print_r($data);
					
					return;
				}
			}
			$data[] = array('count' => 1, 'id' => $id);
		\Session::put('cart', $data );
	}

    public static function add_to_cart2($id) {

        $id = intval($id);
        $data = \Session::get('cart') ? \Session::get('cart') : [];

        foreach($data as &$p)
        {
            if($p['id'] == $id)
            {
                $p['count']++;
                \Session::put('cart', $data );
                //print_r($data);

                return;
            }
        }
        $data[] = array('count' => 1, 'id' => $id);
        \Session::put('cart', $data );
    }
	public function clear_cart(Request $request) {
		
		\Session::put('cart', [] );
		echo 1;
	}
}
