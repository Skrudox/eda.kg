<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\Media;

/**
 * Class Product
 *
 * @package App
 * @property string $guid
 * @property string $name
 * @property tinyInteger $isgroup
 * @property tinyInteger $isdeleted
 * @property string $parentguid
*/
class Product extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $fillable = ['guid', 'name', 'isgroup', 'isdeleted', 'parentguid_id'];
    protected $hidden = [];
    
    

    /**
     * Set to null if empty
     * @param $input
     */
    public function setParentguidIdAttribute($input)
    {
        $this->attributes['parentguid_id'] = $input ? $input : null;
    }
    
    public function parentguid()
    {
        return $this->belongsTo(Category::class, 'parentguid_id', 'guid')->withTrashed();
    }
	
	public function price() {
		
		return $this->hasOne(Price::class, 'guid', 'guid');
		
		
	}
    /*
	public function myMedia() {

        return $this->hasMany(Media::class, 'model_id', 'id');
    }
    */
}
