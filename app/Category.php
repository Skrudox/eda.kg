<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 *
 * @package App
 * @property integer $basecode
 * @property string $guid
 * @property string $name
 * @property double $mindeliveryordersum
 * @property double $deliverysum
 * @property integer $deliverytime
 * @property time $opentime
 * @property time $closetime
 * @property tinyInteger $monworkingday
 * @property tinyInteger $tueworkingday
 * @property tinyInteger $wedworkingday
 * @property tinyInteger $thuworkingday
 * @property tinyInteger $friworkingday
 * @property tinyInteger $satworkingday
 * @property tinyInteger $sunworkingday
 * @property string $logo
*/
class Category extends Model
{
    use SoftDeletes;

    protected $fillable = ['basecode', 'guid', 'name', 'url', 'mindeliveryordersum', 'deliverysum', 'deliverytime', 'opentime', 'closetime', 'monworkingday', 'tueworkingday', 'wedworkingday', 'thuworkingday', 'friworkingday', 'satworkingday', 'sunworkingday', 'logo'];
    protected $hidden = [];
    

    public function generateWorkingDays() {

        $arr = [];
        $arr['пн'] = $this->monworkingday;
        $arr['вт'] = $this->tueworkingday;
        $arr['ср'] = $this->wedworkingday;
        $arr['чт'] = $this->thuworkingday;
        $arr['пт'] = $this->friworkingday;
        $arr['сб'] = $this->satworkingday;
        $arr['вс'] = $this->sunworkingday;
        $str = '';
        $i = 0;
        $c = count($arr);
        foreach($arr as $key => $a)
        {
            if($a)
            {

                if($c == $i + 1)
                {
                    $str .= $key;
                }
                else
                {
                    $str .= $key.', ';
                }

            }
            $i++;
        }

       return $str;


    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setBasecodeAttribute($input)
    {
        $this->attributes['basecode'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setMindeliveryordersumAttribute($input)
    {
        if ($input != '') {
            $this->attributes['mindeliveryordersum'] = $input;
        } else {
            $this->attributes['mindeliveryordersum'] = null;
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDeliverysumAttribute($input)
    {
        if ($input != '') {
            $this->attributes['deliverysum'] = $input;
        } else {
            $this->attributes['deliverysum'] = null;
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDeliverytimeAttribute($input)
    {
        $this->attributes['deliverytime'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setOpentimeAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['opentime'] = Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            $this->attributes['opentime'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getOpentimeAttribute($input)
    {
        if ($input != null && $input != '') {
            return Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            return '';
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setClosetimeAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['closetime'] = Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            $this->attributes['closetime'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getClosetimeAttribute($input)
    {
        if ($input != null && $input != '') {
            return Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            return '';
        }
    }

    public function products()
    {

        return $this->hasMany(Product::class, 'parentguid_id', 'guid')->where('isgroup', 0)->withoutTrashed();
    }
    public function foodTypes()
    {
        return $this->hasMany(CategoryType::class, 'guid', 'guid')->where('type', 'food')->get();
    }
    public function kitchenTypes()
    {
        return $this->hasMany(CategoryType::class, 'guid', 'guid')->where('type', 'kitchen')->get();
    }
    
}
