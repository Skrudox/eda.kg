<?php

namespace App\Providers;

use App\Role;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();

        $access = [1];
        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Roles
        Gate::define('role_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('role_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('role_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('role_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('role_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('user_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('user_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('user_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('user_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });

        // Auth gates for: Products
        Gate::define('product_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('product_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('product_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('product_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('product_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });

        // Auth gates for: Categories
        Gate::define('category_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('category_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('category_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('category_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('category_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });

        // Auth gates for: Prices
        Gate::define('price_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('price_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('price_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('price_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('price_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });

        // Auth gates for: Orders
        Gate::define('order_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('order_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('order_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('order_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('order_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });

        // Auth gates for: Orders
        Gate::define('page_access', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('page_create', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('page_edit', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('page_view', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
        Gate::define('page_delete', function ($user) use($access) {
            return in_array($user->role_id, $access);
        });
    }
}
