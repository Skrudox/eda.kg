<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = ['_token'];

    public function getAuthor(){


        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
