@if (!isset($is_ajax) || $is_ajax == 0)
<!DOCTYPE html>
<html>
	<head>
		@if(isset($title))

			<title>{{$title}}</title>

		@endif
		<meta charset="utf-8" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/css/main.css" rel="stylesheet" />
		<!--<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>-->
		<script
				src="https://code.jquery.com/jquery-3.3.1.js"
				integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
				crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="/js/angular.min.js" ></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/css/slick-theme.css"/>
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

		<script>
			var firstSearch = true;
		$.expr[":"].contains = $.expr.createPseudo(function(arg) {
			return function( elem ) {
				return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
			};
		});
		function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);

			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};
		$.ajaxSetup({
		  headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
		function updateCart() {
			
				var request = $.ajax({
						url: "/cart-data",
						type: "get",
						success: function(data) {
							
							//console.log(data);
							$('.cart_icon_c').hide();

							$('.cart').html(data).show();
							//alert(1);
                            $('.cart').css({width: '100%', top: 'auto', left: 'auto', height: 'auto', 'max-height': '800px'});
						}
					});
		}

            function removeURLParameter(url, parameter) {
                //prefer to use l.search if you have a location/link object
                var urlparts= url.split('?');
                if (urlparts.length>=2) {

                    var prefix= encodeURIComponent(parameter)+'=';
                    var pars= urlparts[1].split(/[&;]/g);

                    //reverse iteration as may be destructive
                    for (var i= pars.length; i-- > 0;) {
                        //idiom for string.startsWith
                        if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                            pars.splice(i, 1);
                        }
                    }

                    url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
                    return url;
                } else {
                    return url;
                }
            }

            var parseQueryString = function (queryString) {
                var params = [], queries, temp, i, l;
                queryString = queryString.split('?')[1];
                // Split into key/value pairs
                if (!queryString) {
                    return [];
                }
                queries = queryString.split("&");
                //console.log(queries.length);
                // Convert the array of strings into an object
                //console.log(queries);
                for (i = 0; i < queries.length; i++) {
                    temp = queries[i].split('=');
                    console.log(temp[0]);
                    if (temp[0] == 'food_type[]' && temp[1]) {
                        params.push(temp[1]);
                    }
                }
                return params;
            };
            var checkeds = parseQueryString(window.location.href);

		$(function() {


			//var leftBar = $('.leftBar').clone();
			//console.log(leftBar.html());
			//$('.master_block').prepend('<div class="leftBar>' + leftBar + '</div>');
            var f_query = getUrlParameter('query');
			console.log(f_query);
            var f_s_type = getUrlParameter('s_type');
            $('.filters input[type="radio"]').change(function () {

                $('.innercontent').html('');
                var placeholder = $(this).data('placeholder');
                $('#main_search').attr('placeholder', placeholder);
                var s = $('form.search').serialize();
                var val = $('.filters input[type="radio"]:checked').val();
                var url = '';
                if (val == 1) {
                    //url = '';
                }
                else if (val == 2) {
                    //url = '';
                }
                else if (val == 3) {
                    //url = '';
                }

                var currentUrl = window.location.href;
                //var currentUrl = removeURLParameter(window.location.href, 'query');
                //currentUrl = removeURLParameter(currentUrl, 's_type');
                var query = (currentUrl.split('?')[1] ? currentUrl.split('?')[1] : '');
                //query += url;
                //console.log(query);
              //  if(!query)
				//{
				var str = '';
                for (var i = 0; i < checkeds.length; i++) {

                    var el = checkeds[i];
                    //console.log(el);
                    if(i == checkeds.length -1)
                    {
                        str += 'food_type[]=' + el;
                    }
                    else
                    {
                        str += 'food_type[]=' + el + '&';
                    }

                }
                if(str) {
                    str += '&';
                }
				    query = str + 'query=' + $('#main_search').val() + '&s_type=' + val;
				//}
				console.log(query);
                history.pushState({urlPath: '/search?' + query}, "", '/search?' + query);
                var request = $.ajax({
                    url: "/ajax_cats",
                    type: "get",
                    data: query + '&firstSearch=' + firstSearch,
                    success: function (data) {


                        $('.innercontent').html(data);
                        $('.master_block').show();
                    }
                });
                if(firstSearch)
                {
                    firstSearch = false;
                    $('.leftBar').show();
                }
            });

            $('#main_search').keyup(function () {

                var val = $(this).val();
                //console.log(val);
                if (val != '') {
                    $('.container').hide();

                    $(".container .c_name:contains('" + val + "')").parent().show();
                }
                else {
                    $('.container').show();
                }
                var currentUrl = removeURLParameter(window.location.href, 'query');
                currentUrl = removeURLParameter(currentUrl, 's_type');
                var s = $('form.search').serialize();
                var f_s_type = getUrlParameter('s_type');
                if(!f_s_type)
				{
				    //alert(f_s_type);
                    $('.filters input[type="radio"][value="' + 1 + '"]').prop('checked', true).change();
				}
                var query =  (currentUrl.split('?')[1] ? currentUrl.split('?')[1] + '&' : '') + s;

                history.pushState({urlPath: '/search?' + query}, "", '/search?' + query);
                var request = $.ajax({
                    url: "/ajax_cats",
                    type: "get",
                    data: query + '&firstSearch=' + firstSearch,
                    success: function (data) {

                        $('.innercontent').html(data);
                        $('.master_block').show();
                    }
                });
				if(firstSearch)
				{
                    firstSearch = false;
                    $('.leftBar').show();
				}
            });

            //$('#main_search').keyup();
            if (f_query) {
                $('#main_search').val(f_query);
                $('.container').show();
            }
            if (f_s_type) {
                $('.filters input[type="radio"][value="' + f_s_type + '"]').prop('checked', true).change();
                $('.container').show();
            }
            else
			{
			    if(window.location.pathname == '/search') {
                    $('.filters input[type="radio"][value="' + 1 + '"]').prop('checked', true).change();
                }
			}


            if (!f_s_type && !f_query) {
                ///$('.container').show();

            }

            $(document).on('click', '.add-to-cart', function () {

                var id = $(this).data('id');
                var data = {id: id};
                var request = $.ajax({
                    url: "/add-to-cart",
                    type: "post",
                    data: data,
                    success: function (data) {

                        updateCart();
                    }
                });
            });
            $(document).on('click', '.clear-cart', function () {

                var id = $(this).data('id');
                var data = {id: id};
                var request = $.ajax({
                    url: "/clear-cart",
                    type: "get",
                    data: data,
                    success: function (data) {

                        updateCart();
                    }
                });
            });
            $(document).on('click', '.cart-plus-item', function (e) {

                e.preventDefault();
                var id = $(this).attr('id').split('-')[3];
                var data = {id: id};
                var request = $.ajax({
                    url: "/cart-plus-item",
                    type: "post",
                    data: data,
                    success: function (data) {

                        updateCart();
                    }
                });
            });
            $(document).on('click', '.cart-minus-item', function (e) {

                e.preventDefault();
                var id = $(this).attr('id').split('-')[3];
                var data = {id: id};
                var request = $.ajax({
                    url: "/cart-minus-item",
                    type: "post",
                    data: data,
                    success: function (data) {

                        updateCart();
                    }
                });
            });
            $(document).on('click', '.cart-delete-item', function (e) {

                e.preventDefault();
                var id = $(this).attr('id').split('-')[3];
                var data = {id: id};
                var request = $.ajax({
                    url: "/cart-delete-item",
                    type: "post",
                    data: data,
                    success: function (data) {

                        updateCart();
                    }
                });
            });
            $(document).on('click', '.hide-cart-button', function (e) {

                e.preventDefault();
                $('.cart').hide();
                $('.cart_icon_c').show();
            });

            /*
            $('.cart').draggable({
                //axis: "x",
                drag: function( event, ui ) {
                  $(this).css({
                    "right": "auto",
                    "bottom": "auto"
                  });
                },
                stop: function (event, ui) {
                  $(this).css({
                    "right": ($(window).width() - ($(this).offset().left + $(this).outerWidth())),
                    "bottom": ($(window).height() - ($(this).offset().top + $(this).outerHeight())),
                    "left": "auto",
                    "top": "auto"
                  });
                }
                });
                */


            $('.cart_icon_c').click(function () {

                $(this).hide();
                $('.cart').show();


            });

            $(document).on('click', '.checkout-cart', function (e) {


                e.preventDefault();
                $('.cart').css({width: '100%', top: 0, left: 0, height: '100%', 'max-height': '100%'});
                var request = $.ajax({
                    url: "/checkout-cart",
                    type: "get",
                    success: function (data) {

                        $('.cart').html(data);
                    }
                });


            });

            $(document).on('click', '.back-cart-button', function (e) {

                e.preventDefault();

                updateCart();


            });
            /*
            $('.food-types-filter input').click(function(){

                var ch = $(this).prop('checked');
                $('.food-types-filter input').serialize();


            });
            */



            //checkeds.pop();
            console.log(checkeds);
            if (checkeds.length == 0) {
                var c = $("input[id='food-type-0']:checked").val();
                if (c != 0) {
                    checkeds.push(c);
                }
                //console.log(checkeds);


            }
            else
			{
                $("input[id^='food-type-0']:checked").prop('checked', false);
			    for(var i = 0; i < checkeds.length; i++)
				{
					(function(i) {
                        var val = checkeds[i];
                        //console.log('input[id="food-type-' + val + '"]');

                        $('input[id="food-type-' + val + '"]').prop('checked', true);
                    })(i);
				}
			}

            $('body').on('change', "input[id^='food-type-']", function () {
                var food_types = $("#food-type-filter").serialize();
                var newurl;
                var description;
                var s = $('form.search').serialize();
                newurl = '' + s;
                if ($(this).attr('id') == 'food-type-0') {
                    food_types = '';
                    //newurl = '/search?' + s;
					//alert(1);
                    $("input[id^='food-type-']").prop('checked', false);
                    $('.eda_sidebar_widget_search p').text("Выберите категорию для описания списка заведений");
                    $(this).prop('checked', true);
                    checkeds = [];
                    //newurl = '/search?' + newurl;
                }
                else {
                    var idx = $(this).attr('id').split('-')[2];
					//var idx = $(this).val();
                    newurl = '' + decodeURIComponent(food_types);
                    $('#food-type-0').prop('checked', false);
                    if ($(this).prop('checked')) {
                        //alert(1);
                        checkeds.push(idx);
                        description = $(this).data('description');
                        $('.eda_sidebar_widget_search p').text(description);
                        var str = '';
                        //alert(checkeds.length);
                        for (var i = 0; i < checkeds.length; i++) {

                            var el = checkeds[i];
                            console.log(el);
                            if(i == checkeds.length -1)
							{
                                str += 'food_type[]=' + el;
							}
							else
							{
                                str += 'food_type[]=' + el + '&';
							}

                        }
                        if(s)
						{
						    str += '&';
						}
                        food_types = str;
                       // alert(str);
                        newurl = '' + decodeURIComponent(food_types);
						//alert(checkeds.length);
                    }
                    else {
                        var index = checkeds.indexOf(idx);
                        checkeds.splice(index, 1);
                        var d = checkeds[checkeds.length - 1];
                        var last = $("input[id='food-type-" + d + "']");
                        description = last.data('description');
                        //console.log(d);

                        $('.eda_sidebar_widget_search p').text(description);
                        var str = '';
						//if(checkeds.length == 1)
						//{
						//    checkeds = [];
						//}
						//console.log(str);
                        //console.log(checkeds.length);
                        for (var i = 0; i < checkeds.length; i++) {

                            var el = checkeds[i];
                            //console.log(el);
                            str += 'food_type[]=' + el + '&';
                        }
                       // alert(str);
                        food_types = str;
                        newurl = '' + decodeURIComponent(food_types);
                        //alert(newurl);
                        //alert(0);
                    }
                   // if (checkeds.length == 1) {
                       // var d = checkeds[0];
                       // var last = $("input[id='food-type-" + d + "']");
                       // newurl = last.data('altname') + '/search?' + s;
						//alert(newurl);
                       newurl += s;
                    //alert(s);
                   // }
                   // else
					//{
                       // newurl =  s;
					//}
					//alert(checkeds.length);
                    if (checkeds.length == 0) {
                       // alert(2);
                       // newurl = '';
                        $("input[id^='food-type-']").prop('checked', false);
                        $("input[id='food-type-0']").prop('checked', true);
                       $('.eda_sidebar_widget_search p').text("Выберите категорию для описания списка заведений");
//
                    }


                }
				//newurl += s;
                //console.log(newurl);
                //var s = $($('.search_form')).serialize();
               // var query =  '/search?' + currentUrl.split('?')[1] + '&' + s;
               //var data = currentUrl.split('?')[1] + '&' + s;
				//alert(newurl);
                var request = $.ajax({
                    url: "/ajax_cats",
                    type: "get",
                    data: newurl,
                    success: function (data) {

                        $('.innercontent').html(data);
                        $('.master_block').show();
                    }
                });
                window.history.pushState({path: '/search?' + newurl},'','/search?' + newurl);
            });


            $(document).on('change', '.cart input[name="delivery_type"]', function(){

				var el = $(this);
                var val = el.prop('checked');
                if(val && el.val() == 2)
				{
					$('.cart .address_map, .cart input[name="address"], .address_label').hide();
				}
				else
                if(val && el.val() == 1)
                {
                    $('.cart .address_map, .cart input[name="address"], .address_label').show();
                }


			});

            $(document).on('click', '.cart-submit', function(e){

               // e.preventDefault();
                var data = $('.cart form').serialize();
                console.log(data);
               // $('.cart form').submit();
				/*
                var request = $.ajax({
                    url: "/cart-submit",
                    type: "post",
                    data: data,
                    success: function (data) {

                        console.log(data);
                       // $('.master_block').html(data);
                       // $('.master_block').show();
                    }
                });*/

			});

            $('form.search').submit(function(e){

                e.preventDefault();


			});

            $('.dark').click(function(){


                $(this).hide();
			})

        });
		</script>
	</head>
	<body>
        <div class="main_parallax"></div>
        <div class="topgradient"></div>
		<div class="header">
			
			<div class="search_form">
				<div class="logo">
					<a href="{{url('/')}}">
						<img src="https://test1.eda.kg/images/logo.png" />
					</a>
				</div>
				<div class="phone_h">
					<span style="color: white;">+996 (312)</span> <strong style="color: #ff7546;">47-47-47</strong> или <a class="back_call" href="">закажите обратный звонок</a>
				</div>
				<div class="user_info">
					@if($user = Auth::user())
						Вы авторизованы как <strong>"{{$user->name}}"</strong>
						<form method="post" action="/logout" style="display: inline;margin-bottom: 10px;">
							{!! Form::token() !!}
							<div class="logout_button"><button>Выйти</button></div>
						</form>
						@else
						Вы уже заказывали у нас? <a href="{{url('/login')}}">Войти</a>
						@endif
				</div>
			<form method="post" class="search">
			  <input type="text" class="textbox form-control" id="main_search" name="query" placeholder="Искать еду">

			  <div class="filters">
					<label class="rcontainer">
					Еда
					  <input type="radio" data-placeholder="Искать еду" value="1" name="s_type" >
					  <span class="checkmark"></span>
					</label>
				  <!--
					<label class="rcontainer">

					Кухни
					  <input type="radio" data-placeholder="Искать кухни" value="2" name="s_type" >
					  <span class="checkmark"></span>

					</label>
					-->
					<label class="rcontainer">
					Заведения
					  <input type="radio" data-placeholder="Искать заведения" value="3" name="s_type" >
					  <span class="checkmark"></span>
					</label>
			</form>
                <a class="popular_orders" href="{{url('/popular_orders')}}">Популярные заказы</a>
					<!--
				<input type="radio" data-placeholder="Искать еду" checked value="1" name="s_type" /> <span>еда</span>
				<input type="radio" data-placeholder="Искать кухни" value="2" name="s_type" /> <span>Кухни</span>
				<input type="radio" data-placeholder="Искать заведения" value="3" name="s_type" /> <span>заведения</span>
				-->
			  </div>
			</div>
		</div>
			<div class="content">

               <!-- <div ng-app="mainrows" ng-controller="myCtrl" class="master_block">-->
				<div class="leftBar">

					<div class="eda_sidebar_widget">
						<div class="eda_sidebar_widget_title">Выберите кухню</div>
						<div class="eda_sidebar_widget_search clearfix">

							<ul class="food-types-filter clearfix">
								<li><input checked type="checkbox" data-altname="all" name="food_type[]" id="food-type-0" value="0"><label for="food-type-0">Все кухни</label></li>
								@foreach($kitchenTypes as $f)
									<li><input type="checkbox" data-altname="{{$f->name}}" name="food_type[]" id="food-type-{{$f->id}}" value="{{$f->name}}"><label for="food-type-{{$f->id}}">{{$f->name}}</label></li>
								@endforeach
								<!--
								<li><input type="checkbox" data-altname="national" name="food_type[]" id="food-type-1" value="1"><label for="food-type-1">Национальная кухня</label></li>

								<li><input type="checkbox" data-altname="chinese" name="food_type[]" id="food-type-2" value="2"><label for="food-type-2">Китайская кухня</label></li>

								<li><input type="checkbox" data-altname="european" name="food_type[]" id="food-type-3" value="3"><label for="food-type-3">Европейская кухня</label></li>

								<li><input type="checkbox" data-altname="exotic" name="food_type[]" id="food-type-9" value="9"><label for="food-type-9">Экзотическая кухня</label></li>
								-->
							</ul>

						</div>
					</div>
					<div class="eda_sidebar_widget">
						<div class="eda_sidebar_widget_title">Выберите тип блюда</div>
						<div class="eda_sidebar_widget_search clearfix">

							<ul class="food-types-filter clearfix">
								@foreach($foodTypes as $f)
									<li><input type="checkbox" data-altname="{{$f->name}}" name="food_type[]" id="food-type-{{$f->id}}" value="{{$f->id}}"><label for="food-type-{{$f->id}}">{{$f->name}}</label></li>
								@endforeach
								<!--
								<li><input type="checkbox" data-altname="fast-food" name="food_type[]" id="food-type-10" value="10"><label for="food-type-10">Фаст фуд</label></li>
								<li><input type="checkbox" data-altname="pizza" name="food_type[]" id="food-type-14" value="14"><label for="food-type-14">Пицца</label></li>

								<li><input type="checkbox" data-altname="shashlik" name="food_type[]" id="food-type-5" value="5"><label for="food-type-5">Шашлык</label></li>

								<li><input type="checkbox" data-altname="sushi" name="food_type[]" id="food-type-6" value="6"><label for="food-type-6">Суши</label></li>

								<li><input type="checkbox" data-altname="pie" name="food_type[]" id="food-type-11" value="11"><label for="food-type-11">Выпечка</label></li>
								-->
							</ul>

						</div>
					</div>
					<div class="l_desc">
						<div class="t">Описание доставки</div>
						<div class="d">
							Текст текст Текст текст Текст текст Текст текст Текст текст
							Текст текст Текст текст Текст текст Текст текст Текст текст
							Текст текст Текст текст Текст текст Текст текст Текст текст
							Текст текст Текст текст Текст текст Текст текст Текст текст
							Текст текст Текст текст Текст текст Текст текст Текст текст
							Текст текст Текст текст Текст текст Текст текст Текст текст
						</div>
					</div>

				</div>
                    <div class="innercontent">
						@endif
		        @yield('content')
						@if (isset($is_ajax) && $is_ajax == 0)
                    </div>
                <!--</div>-->
				<div class="clear"></div>


			</div>



		<div class="cart">
			{!! App\Http\Controllers\HomeController::cart_data2() !!}
		</div>
		<div class="cart_icon_c">
			<img src="{{url('/cart_icon.png')}}" class="cart_icon" />
			<div class="circle"></div>
		</div>
		<div class="footer">

			<ul class="bot_links">
				@foreach($pages as $page)
					<li>
						<a href="/page/{{$page->url}}">{{$page->title}}</a>
					</li>
				@endforeach
				<!--
				<li>
					<a href="/page/about-project">О проекте</a>
				</li>
				<li>
					<a href="/page/blog">Блог</a>
				</li>
				<li>
					<a href="/page/partners">Партнёрам</a>
				</li>
				<li>
					<a href="/page/how-it-works">Как это работает</a>
				</li>
				<li>
					<a href="/page/how-to-order">Как заказать</a>
				</li>
				<li>
					<a href="/page/delivery-and-payment">Доставка и оплата</a>
				</li>
				-->
			</ul>
		</div>
		@if(\Session::has('message'))
				<div class="dark">
					<div class="infowin">
						{{\Session::get('message')}}
						@php
							\Session::forget('message');

						@endphp
					</div>

				</div>
		@endif
	</body>
</html>
@endif