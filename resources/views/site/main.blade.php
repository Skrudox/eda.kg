@extends('layouts.site')

@section('content')
    <script>
        var app = angular.module("mainrows", [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
        var s_type = 1;
        app.controller("myCtrl", function($scope) {
            $scope.kitchens = [
                {img: "/images/food_types/book_all.png", title: 'Весь каталог'},
                {img: "/images/food_types/europe.png", title: 'Национальная кухня', params: '?food_type[]=2&query=&s_type=' + s_type},
                {img: "/images/food_types/china.png", title: 'Китайская пицца', params: '?food_type[]=3&query=&s_type=' + s_type},
                {img:  "/images/food_types/europe.png", title: 'Европейская кухня', params: '?food_type[]=4&query=&s_type=' + s_type},
                {img:  "/images/food_types/exotic.png", title: 'Экзотическая кухня', params: '?food_type[]=5&query=&s_type=' + s_type}
            ];
            $scope.foodTypes = [

                {img: '/images/food_types/book_all.png', title: 'Весь каталог', params: '?food_type[]=0&query=&s_type=' + s_type},
                {img: '/images/food_types/fastfood.png', title: 'Фаст фуд', params: '?food_type[]=6&query=&s_type=' + s_type},
                {img: '/images/food_types/pizza.png', title: 'Пицца', params: '?food_type[]=7&query=&s_type=' + s_type},
                {img: '/images/food_types/pizza.png', title: 'Шашлык', params: '?food_type[]=8&query=&s_type=' + s_type},
                {img: '/images/food_types/sushi.png', title: 'Суши', params: '?food_type[]=9&query=&s_type=' + s_type},
                {img: '/images/food_types/pie.png', title: 'Выпечка', params: '?food_type[]=10&query=&s_type=' + s_type},
            ];
            $scope.places = [

                {img: '/images/places/1.jpg', title: 'one'},
                {img: '/images/places/2.jpg', title: 'two'},
                {img: '/images/places/3.jpg', title: 'three'},
                {img: '/images/places/4.jpg', title: 'four'},
                {img: '/images/places/5.jpg', title: 'five'},
                {img: '/images/places/6.jpg', title: 'six'},
            ];
            $scope.stocks = [

                {img: '/images/places/1.jpg', title: 'one'},
                {img: '/images/places/2.jpg', title: 'two'},
                {img: '/images/places/3.jpg', title: 'three'},
                {img: '/images/places/4.jpg', title: 'four'},
                {img: '/images/places/5.jpg', title: 'five'},
                {img: '/images/places/6.jpg', title: 'six'},
            ];
        });
    </script>
    <style>
        .master_block {

            width: 100%;
        }
    </style>
    <div ng-app="mainrows" ng-controller="myCtrl" class="master_block">

        <div class="main_rows">
            <!--
            <div class="main_blocks">

                <div class="mb" ng-repeat="x in kitchens">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
            -->
            <div>
                <h2>Кухни</h2>
            </div>
            <div class="for_slider">
                <div class="mb" ng-repeat="x in kitchens">
                    <a href="/search<%x.params%>">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                    </a>
                </div>
            </div>
            <div>
                <h2>Типы блюд</h2>
            </div>
            <div class="for_slider">
                <div class="mb" ng-repeat="x in foodTypes">
                    <a href="/search<%x.params%>">
                    <img src="<%x.img%>" />
                    </a>
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
            <div>
                <h2>Заведения</h2>
            </div>
            <div class="for_slider">
                <div class="mb" ng-repeat="x in places">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
            <div>
                <h2>Акции</h2>
            </div>
            <div class="for_slider">
                <div class="mb" ng-repeat="x in places">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
            <!--
            <div>
                1
            </div>

            <div>
                2
            </div>

            <div>
                3
            </div>
            -->
        </div>
        <!--
        <div class="main_rows">
            <div class="main_blocks">
                <div>
                    <h2>Типы блюд</h2>
                </div>
                <div class="mb" ng-repeat="x in foodTypes">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_rows">
            <div class="main_blocks">
                <div>
                    <h2>Заведения</h2>
                </div>
                <div class="mb" ng-repeat="x in places">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_rows">
            <div class="main_blocks">
                <div>
                    <h2>Акции</h2>
                </div>
                <div class="mb" ng-repeat="x in places">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
-->
        <div class="clear"></div>
    </div>
    </div>
    <script>
        $(function() {
            $('.for_slider').slick({
                dots: true,
               // prevArrow: false,
               // nextArrow: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                centerMode: true,
                variableWidth: true
            });
        });
    </script>
@endsection
