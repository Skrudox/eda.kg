
@extends('layouts.site', ['title' => $category->name])

@section('content')
<div class="place_info">
<h1 class="cat_title">{{$category->name}}</h1>
    <div class="place_logo">
        <img src="/uploads/logos/{{$category->logo}}" />
    </div>
    <div class="place_stats2">
        <div>Время открытия: <strong>{{$category->opentime}}</strong></div>
        <div>Время закрытия: <strong>{{$category->closetime}}</strong></div>
        <div>
            Время работы: <strong>{{$working_days}}</strong>
        </div>
    </div>
    <div class="place_stats3">
        <div>
            Кухни:<strong>
        @foreach($category->foodTypes() as $f)

            {{$f->name}}
            @if(!$loop->last)
                /
            @endif
        @endforeach
            </strong>
        </div>
        <div>
        Типы кухни:<strong>
        @foreach($category->kitchenTypes() as $f)

            {{$f->name}}
            @if(!$loop->last)
                /
            @endif
        @endforeach
            </strong>
        </div>
    </div>
    <div class="place_stats">
        <div class="stats_minsum">Минимальная сумма заказа: <strong>{{$category->mindeliveryordersum}} сом</strong></div>
        <div class="stats_deliverysum">Стоимость доставки: <strong>{{$category->deliverysum}} сом</strong></div>
        <div class="stats_deliverytime">Время доставки: <strong>{{$category->deliverytime}} минут</strong></div>
    </div>
</div>
<div class="category_tab">
    <a class="@if(Request::route('sub') == '') active @endif" href="/category/{{Request::route('url')}}">Продукты</a>
    <a class="@if(Request::route('sub') == 'otzyvy') active @endif otzyvy" href="/category/{{Request::route('url')}}/otzyvy">Отзывы</a>
    
    <div class="clear"></div>
</div>

@if(isset($comments))

    {!! $comments !!}
@endif


@if (!isset($comments) && isset($category->products) && count($category->products) > 0 && !$is_ajax)
    @foreach ($category->products as $product)


        <div class="container">
            <div class="c_n_p">
                <div class="c_name">{{$product->name}}</div>
                <div class="c_price">
                    @if($product->price)
                        {{$product->price->price}} сом
                    @elseif(!$product->isgroup)
                        0 сом
                    @endif
                </div>
                <div>
                    Заказано: {{$product->ordered_times}} раз
                </div>
            </div>
            <div class="clear"></div>
            <div class="c_img">

                <a>
                    <img src="{{$product->getFirstMediaUrl('photos')}}" />
                </a>
                <!--
                @if($product->parentguid)
                    <a class="product_cat_logo" href="{{url('/category/'.$product->parentguid->url)}}">
                        <img src="{{ env('APP_URL') . '/public/uploads/logos/'.$product->parentguid->logo }}" />
                    </a>
                @endif
                -->
            </div>
            <div>
            @if($product->parentguid)
                <!--
								<strong>{{$product->parentguid->name}}</strong>

								<a href="{{url('/pruduct/'.$product->id)}}">
									<img src="{{ env('APP_URL') . '/public/uploads/logos/'.$product->parentguid->logo }}" />
								</a>
								-->
                @endif
                <div>
                    @if(!$product->isgroup)
                        <button id="product_{{$product->id}}" data-id="{{$product->id}}" class="add-to-cart myButton">Добавить в корзину</button>
                    @endif
                </div>
            </div>

        </div>

    @endforeach
@else
    <!--Нет продуктов-->
@endif
<div class="clear"></div>

@stop