
@extends('layouts.site', ['title' => 'Популярные заказы'])

@section('content')

    <h1>Популярые заказы</h1>
    <div>
        Выберите готовый набор наиболее часто заказываемых продуктов
    </div>
    <div class="popular_items">
        @foreach($orders as $key => $o)

            <div id="popular_order_{{$o->id}}" class="popular_order">

                <h2>Набор: {{$key + 1}}</h2>
                <div class="scroll_popular_items">
                    @php
                    $totalSum = 0;
                    @endphp
                @foreach($o->products as $p)
                    <div class="popular_item">
                        <img src="{{$p->getFirstMediaUrl('photos')}}" />
                        <div>
                        {{$p->name}}
                        @if($p->price)
                        ,
                        {{$p->price->price}} сом
                            @php
                                $totalSum += $p->price->price;
                            @endphp
                        @endif
                        </div>
                    </div>
                @endforeach
                </div>
                    <div class="clear"></div>
                <div class="popular_ordered_times">
                    <strong>Сумма: {{$totalSum}} сом</strong>,
                    <strong>Заказан: {{$o->count}} раз</strong>
                </div>

                <div class="popular_button_choose">
                    <button class="choose-popular myButton" onclick="window.location.href = '/popluar_orders/get/{{$o->id}}';">Выбрать</button>
                </div>
            </div>
        @endforeach

    </div>
    <div class="clear"></div>

@stop