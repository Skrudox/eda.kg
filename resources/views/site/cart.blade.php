	<a class="hide-cart-button" href=""><img src="/images/closebtn.png" /></a>
<h2>Корзина</h2>
				  @if (Session::get('cart') && count(Session::get('cart')) > 0)

					  @php
					  $totalSum = 0;
					  @endphp
@foreach ($products as $product)
	@php
	$sum = $product['price'] * $product['count'];
	$totalSum += $sum;
	@endphp
<div>
	<strong>
		<a href="" class="cart-plus-item" id="cart-plus-item-{{$product['id']}}">+</a>
		<a href="" class="cart-minus-item" id="cart-minus-item-{{$product['id']}}">-</a>



	<img src="{{$product['img']}}" style="width: 30px;" /> {{$product['name']}} x{{$product['count']}} = {{$sum}} сом
		<a class="cart-delete-item" id="cart-delete-item-{{$product['id']}}" href="">x</a>
	</strong>
	<div class="clear"></div>
</div>
@endforeach
<hr />
<div class="totalSum">Итого к оплате: <strong>{{$totalSum}} сом</strong></div>
<button class="checkout-cart myButton">Оформить заказ</button>	
<button class="clear-cart myButton">Очистить корзину</button>
<style>
	.cart_icon_c {

		display: none;

	}
</style>
@else
	<div>
	Нет товаров в корзине
	</div>
	<style>
	.cart {

		display: none;

	}
	</style>
@endif