		<a class="back-cart-button" href=""><img src="/images/back_icon.png" /></a> <a class="hide-cart-button" href=""><img src="/images/closebtn.png" /></a>
<h2>Оформление заказа</h2>
<form method="post" action="{{url('/cart-submit')}}">
	{!! Form::token() !!}
	<p>Тип доставки</p>
	<label class="rcontainer">
		Доставка
		<input type="radio" checked value="1" name="delivery_type" >
		<span class="checkmark"></span>
	</label>
	<label class="rcontainer">
		Самовывоз
		<input type="radio"value="2" name="delivery_type" >
		<span class="checkmark"></span>
	</label>
	<p class="address_label">Адрес</p>
	<input name="address" type="text" class="form-control" required />
    <input name="address_coords" type="hidden" classs="form-control" id="address_coords" />
	<p>Телефон</p>
	<input name="phone" required type="text" class="form-control" />
	<p><strong>Вам будет отправлено смс с информацией о вашем заказе</strong></p>
	<div style='overflow:hidden;height:644px;width:100%; margin-top: 20px;' class="address_map">
		<div id='gmap_canvas' style='height:644px;width:100%;'>

		</div>
		</div>
		<style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div>
	<script>

            function init_map() {
                var myOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng(42.877087, 74.587398),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
               // console.log(123);
                var map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                var geocoder = new google.maps.Geocoder;
				var lastMarker = null;
                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                        infoWindow.setPosition(pos);
                        infoWindow.setContent('Location found.');

                        infoWindow.open(map);
                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, infoWindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
                google.maps.event.addListener(map,'click',function(event) {
                    var str = '';
                    geocoder.geocode({'location': {lat: event.latLng.lat(), lng: event.latLng.lng()}}, function(results, status) {
                        if (status === 'OK') {
                            if (results[0]) {

                                str += results[0].formatted_address;
                                //str += ', ' + event.latLng.lng();
                                if(lastMarker)
                                {
                                    lastMarker.setMap(null);
                                }

                                $('#address_coords').val(event.latLng.lat() + ',' + event.latLng.lng());
                                lastMarker = new google.maps.Marker({
                                    position: {lat: event.latLng.lat(), lng: event.latLng.lng()},
                                    map: map,
                                    title: results[0].formatted_address
                                });


                                infowindow = new google.maps.InfoWindow({
                                    content: '<strong>' + results[0].formatted_address + '</strong>'
                                });
                                infowindow.open(map, lastMarker);
                                $('.cart input[name="address"]').val(str);
                            }
                        }
                    });


                });
                //infowindow = new google.maps.InfoWindow({
                //   content: '<strong>Title</strong><br>London, United Kingdom<br>'
               // });
               // google.maps.event.addListener(marker, 'click', function () {
               //     infowindow.open(map, marker);
               // });

            }

           init_map();



	</script>
	<!--
	<p>Имя</p>
	<input name="name" required type="text" class="form-control" />
	-->

	<!--
	<p>E-mail</p>
	<input name="email" required type="email" class="form-control" />
	<p>Сдачи с</p>
	<input name="money_return" required type="number" class="form-control" />
	<p>Время доставки</p>
	<input name="delivery_datetime" type="datetime-local" class="form-control" />
	<p>Тип оплаты</p>
	<select name="payment_type" class="form-control">
		<option value="1">Наличные</option>
	</select>
	-->
	<!--
	<p>Комментарий</p>
	<textarea name="comment" type="email" class="form-control" ></textarea>
	-->
	<p><button class="cart-submit myButton">Отправить</button></p>
</form>