@if (count($products) > 0)
    @foreach ($products as $product)


        <div class="container">
            <div class="c_name">{{$product->name}}</div>
            <div class="c_price">
                @if($product->price)
                    {{$product->price->price}} сом
                @elseif(!$product->isgroup)
                    0
                @endif
            </div>
            <div class="clear"></div>
            <div class="c_img">
                <a href="{{url('/pruduct/'.$product->id)}}">
                    <img src="{{$product->getFirstMediaUrl('photos')}}" />
                </a>
                @if($product->parentguid)
                    <a class="product_cat_logo" href="{{url('/category/'.$product->parentguid->id)}}">
                        <img src="{{ env('APP_URL') . '/public/uploads/logo/'.$product->parentguid->logo }}" />
                    </a>
                @endif
            </div>
            <div>
            @if($product->parentguid)
                <!--
								<strong>{{$product->parentguid->name}}</strong>

								<a href="{{url('/pruduct/'.$product->id)}}">
									<img src="{{ env('APP_URL') . '/public/uploads/logos/'.$product->parentguid->logo }}" />
								</a>
								-->
                @endif
                <div>
                    @if(!$product->isgroup)
                        <button id="product_{{$product->id}}" data-id="{{$product->id}}" class="add-to-cart myButton">Добавить в корзину</button>
                    @endif
                </div>
            </div>

        </div>

    @endforeach
@else
    Нет продуктов
@endif
<div class="clear"></div>