
@extends('layouts.site')
@section('content')

				    @if (count($categories) > 0)
                        @foreach ($categories as $category)
					
						<div class="container">
							<div class="c_name">{{$category->name}}</div>
							<div class="clear"></div>
							<div class="c_img">
								<img src="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}" />
							</div>
							<div>
								<button onclick="window.location.href = '/category/{{$category->id}}}'" id="category_{{$category->id}}" data-id="{{$category->id}}" class="add-to-cart myButton">Перейти в заведение</button>
							</div>
						</div>
					
						@endforeach
                    @else
						Нет категорий
                    @endif
			<div class="clear"></div>
			</div>
@stop