@extends('layouts.site')

@section('content')
    <script>
        var app = angular.module("mainrows", [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
        app.controller("myCtrl", function($scope) {
            $scope.kitchens = [
                {img: "/images/food_types/book_all.png", title: 'Весь каталог'},
                {img: "/images/food_types/europe.png", title: 'Национальная кухня'},
                {img: "/images/food_types/china.png", title: 'Китайская пицца'},
                {img:  "/images/food_types/europe.png", title: 'Европейская кухня'},
                {img:  "/images/food_types/exotic.png", title: 'Экзотическая кухня'}
            ];
            $scope.foodTypes = [

                {img: '/images/food_types/book_all.png', title: 'Весь каталог'},
                {img: '/images/food_types/fastfood.png', title: 'Фаст фуд'},
                {img: '/images/food_types/pizza.png', title: 'Шашлык'},
                {img: '/images/food_types/sushi.png', title: 'Суши'},
                {img: '/images/food_types/pie.png', title: 'Выпечка'},
            ];
            $scope.places = [

                {img: '/images/places/1.jpg', title: 'one'},
                {img: '/images/places/2.jpg', title: 'two'},
                {img: '/images/places/3.jpg', title: 'three'},
                {img: '/images/places/4.jpg', title: 'four'},
                {img: '/images/places/5.jpg', title: 'five'},
                {img: '/images/places/6.jpg', title: 'six'},
            ];
            $scope.stocks = [

                {img: '/images/places/1.jpg', title: 'one'},
                {img: '/images/places/2.jpg', title: 'two'},
                {img: '/images/places/3.jpg', title: 'three'},
                {img: '/images/places/4.jpg', title: 'four'},
                {img: '/images/places/5.jpg', title: 'five'},
                {img: '/images/places/6.jpg', title: 'six'},
            ];
        });
    </script>

    <!--<div ng-app="mainrows" ng-controller="myCtrl" class="master_block">

        <div class="main_rows">
            <div>
                <h2>Кухни</h2>
            </div>
            <div class="main_blocks">
                <div ng-repeat="x in kitchens">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_rows">
            <div>
                <h2>Типы блюд</h2>
            </div>
            <div class="main_blocks">
                <div ng-repeat="x in foodTypes">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_rows">
            <div>
                <h2>Заведения</h2>
            </div>
            <div class="main_blocks">
                <div ng-repeat="x in places">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_rows">
            <div>
                <h2>Акции</h2>
            </div>
            <div class="main_blocks">
                <div ng-repeat="x in places">
                    <img src="<%x.img%>" />
                    <div>
                        <%x.title%>
                    </div>
                </div>
            </div>
        </div>
        -->

        @if (isset($products) && count($products) > 0 && $is_ajax)
            @foreach ($products as $product)


                <div class="container">
                    <div class="c_n_p">
                        <div class="c_name">{{$product->name}}</div>
                        <div class="c_price">
                            @if($product->price)
                                {{$product->price->price}} сом
                            @elseif(!$product->isgroup)
                                0 сом
                            @endif
                        </div>
                        <div>
                            Заказано: {{$product->ordered_times}} раз
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="c_img">

                        <a  id="product_{{$product->id}}" data-id="{{$product->id}}" class="add-to-cart">
                            <img src="{{$product->getFirstMediaUrl('photos')}}" />
                        </a>
                        @if($product->parentguid)
                            <a class="product_cat_logo" href="{{url('/category/'.$product->parentguid->url)}}">
                                <img src="{{ env('APP_URL') . '/public/uploads/logos/'.$product->parentguid->logo }}" />
                            </a>
                        @endif
                    </div>
                    <div>
                    @if($product->parentguid)
                        <!--
								<strong>{{$product->parentguid->name}}</strong>

								<a href="{{url('/pruduct/'.$product->id)}}">
									<img src="{{ env('APP_URL') . '/public/uploads/logos/'.$product->parentguid->logo }}" />
								</a>
								-->
                        @endif
                        <div>
                            @if(!$product->isgroup)
                                <button id="product_{{$product->id}}" data-id="{{$product->id}}" class="add-to-cart myButton">Добавить в корзину</button>
                            @endif
                        </div>
                    </div>

                </div>

            @endforeach
        @else
            <!--Нет продуктов-->
        @endif

        @if (isset($categories) && count($categories) > 0 && $is_ajax)
            @foreach ($categories as $category)

                <div class="container cat">
                    <a href="/category/{{$category->url}}">
                        <div class="c_img">
                            <img src="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}" />
                        </div>
                        <div class="list-cat-info">
                            <div class="c_name">{{$category->name}}</div>
                            <div>Рабочие дни: <strong>{{$category->working_days}}</strong></div>
                            <div>Время открытия: <strong>{{$category->opentime}}</strong></div>
                            <div>Время закрытия: <strong>{{$category->closetime}}</strong></div>
                            <div>
                                Кухни:<strong>
                                    @foreach($category->kitchenTypes() as $f)

                                        {{$f->name}}
                                        @if(!$loop->last)
                                            /
                                        @endif
                                    @endforeach
                                </strong>
                            </div>
                            <div>
                                Типы блюда:<strong>
                                    @foreach($category->foodTypes() as $f)

                                        {{$f->name}}
                                        @if(!$loop->last)
                                            /
                                        @endif
                                    @endforeach
                                </strong>
                            </div>
                            <div class="place_stats">
                                <div class="stats_minsum">Минимальная сумма заказа: <strong>{{$category->mindeliveryordersum}} сом</strong></div>
                                <div class="stats_deliverysum">Стоимость доставки: <strong>{{$category->deliverysum}} сом</strong></div>
                                <div class="stats_deliverytime">Время доставки: <strong>{{$category->deliverytime}}</strong></div>
                            </div>
                        </div>
                    <div>
                        <button onclick="window.location.href = '/category/{{$category->id}}}'" id="category_{{$category->id}}" data-id="{{$category->id}}" class="myButton">Перейти в заведение</button>
                    </div>
                    </a>
                </div>

            @endforeach
        @else
            <!--Нет категорий-->
        @endif
        <div class="clear"></div>
    <!--</div>-->
   <!-- </div>-->
    <style>
        .master_block {

            #width: 100%;
        }
        .leftBar {

            display: block;
        }
    </style>
@endsection
