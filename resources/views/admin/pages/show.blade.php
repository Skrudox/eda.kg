@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.pages.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.pages.fields.title')</th>
                            <td field-key='guid'>{{ $page->title }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pages.fields.url')</th>
                            <td field-key='name'>{{ $page->url }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pages.fields.content')</th>
                            <td field-key='name'>{{ $page->content }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pages.fields.views')</th>
                            <td field-key='name'>{{ $page->views }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pages.fields.user_id')</th>
                            <td field-key='name'>{{ $page->getAuthor->name }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.pages.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
