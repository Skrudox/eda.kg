@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.products.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.products.fields.guid')</th>
                            <td field-key='guid'>{{ $product->guid }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.products.fields.name')</th>
                            <td field-key='name'>{{ $product->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.products.fields.isgroup')</th>
                            <td field-key='isgroup'>{{ Form::checkbox("isgroup", 1, $product->isgroup == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.products.fields.isdeleted')</th>
                            <td field-key='isdeleted'>{{ Form::checkbox("isdeleted", 1, $product->isdeleted == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.products.fields.parentguid')</th>
                            <td field-key='parentguid'>{{ $product->parentguid->guid or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.products.fields.photos')</th>
                            <td field-key='photos'> @foreach($product->getMedia('photos') as $media)
                                <p class="form-group">
                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }} ({{ $media->size }} KB)</a>
                                </p>
                            @endforeach</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.products.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
