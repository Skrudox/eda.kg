@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.categories.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.categories.fields.basecode')</th>
                            <td field-key='basecode'>{{ $category->basecode }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.guid')</th>
                            <td field-key='guid'>{{ $category->guid }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.name')</th>
                            <td field-key='name'>{{ $category->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.mindeliveryordersum')</th>
                            <td field-key='mindeliveryordersum'>{{ $category->mindeliveryordersum }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.deliverysum')</th>
                            <td field-key='deliverysum'>{{ $category->deliverysum }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.deliverytime')</th>
                            <td field-key='deliverytime'>{{ $category->deliverytime }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.opentime')</th>
                            <td field-key='opentime'>{{ $category->opentime }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.closetime')</th>
                            <td field-key='closetime'>{{ $category->closetime }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.monworkingday')</th>
                            <td field-key='monworkingday'>{{ Form::checkbox("monworkingday", 1, $category->monworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.tueworkingday')</th>
                            <td field-key='tueworkingday'>{{ Form::checkbox("tueworkingday", 1, $category->tueworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.wedworkingday')</th>
                            <td field-key='wedworkingday'>{{ Form::checkbox("wedworkingday", 1, $category->wedworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.thuworkingday')</th>
                            <td field-key='thuworkingday'>{{ Form::checkbox("thuworkingday", 1, $category->thuworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.friworkingday')</th>
                            <td field-key='friworkingday'>{{ Form::checkbox("friworkingday", 1, $category->friworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.satworkingday')</th>
                            <td field-key='satworkingday'>{{ Form::checkbox("satworkingday", 1, $category->satworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.sunworkingday')</th>
                            <td field-key='sunworkingday'>{{ Form::checkbox("sunworkingday", 1, $category->sunworkingday == 1 ? true : false, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.categories.fields.logo')</th>
                            <td field-key='logo'>@if($category->logo)<a href="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}" target="_blank"><img src="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}"/></a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="products">
<table class="table table-bordered table-striped {{ count($products) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.products.fields.guid')</th>
                        <th>@lang('quickadmin.products.fields.name')</th>
                        <th>@lang('quickadmin.products.fields.isgroup')</th>
                        <th>@lang('quickadmin.products.fields.isdeleted')</th>
                        <th>@lang('quickadmin.products.fields.parentguid')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($products) > 0)
            @foreach ($products as $product)
                <tr data-entry-id="{{ $product->id }}">
                    <td field-key='guid'>{{ $product->guid }}</td>
                                <td field-key='name'>{{ $product->name }}</td>
                                <td field-key='isgroup'>{{ Form::checkbox("isgroup", 1, $product->isgroup == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='isdeleted'>{{ Form::checkbox("isdeleted", 1, $product->isdeleted == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='parentguid'>{{ $product->parentguid->guid or '' }}</td>
                                <td field-key='photos'>@if($product->photos)<a href="{{ asset(env('UPLOAD_PATH').'/' . $product->photos) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $product->photos) }}"/></a>@endif</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('product_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.products.restore', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('product_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.products.perma_del', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('product_view')
                                    <a href="{{ route('admin.products.show',[$product->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('product_edit')
                                    <a href="{{ route('admin.products.edit',[$product->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('product_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.products.destroy', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="11">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.categories.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
