@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.categories.title')</h3>
    @can('category_create')
    <p>
        <a href="{{ route('admin.categories.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('category_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.categories.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.categories.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($categories) > 0 ? 'datatable' : '' }} @can('category_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('category_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.categories.fields.basecode')</th>
                        <th>@lang('quickadmin.categories.fields.guid')</th>
                        <th>@lang('quickadmin.categories.fields.name')</th>
                        <th>@lang('quickadmin.categories.fields.mindeliveryordersum')</th>
                        <th>@lang('quickadmin.categories.fields.deliverysum')</th>
                        <th>@lang('quickadmin.categories.fields.deliverytime')</th>
                        <th>@lang('quickadmin.categories.fields.opentime')</th>
                        <th>@lang('quickadmin.categories.fields.closetime')</th>
                        <th>@lang('quickadmin.categories.fields.monworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.tueworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.wedworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.thuworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.friworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.satworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.sunworkingday')</th>
                        <th>@lang('quickadmin.categories.fields.logo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($categories) > 0)
                        @foreach ($categories as $category)
                            <tr data-entry-id="{{ $category->id }}">
                                @can('category_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='basecode'>{{ $category->basecode }}</td>
                                <td field-key='guid'>{{ $category->guid }}</td>
                                <td field-key='name'>{{ $category->name }}</td>
                                <td field-key='mindeliveryordersum'>{{ $category->mindeliveryordersum }}</td>
                                <td field-key='deliverysum'>{{ $category->deliverysum }}</td>
                                <td field-key='deliverytime'>{{ $category->deliverytime }}</td>
                                <td field-key='opentime'>{{ $category->opentime }}</td>
                                <td field-key='closetime'>{{ $category->closetime }}</td>
                                <td field-key='monworkingday'>{{ Form::checkbox("monworkingday", 1, $category->monworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='tueworkingday'>{{ Form::checkbox("tueworkingday", 1, $category->tueworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='wedworkingday'>{{ Form::checkbox("wedworkingday", 1, $category->wedworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='thuworkingday'>{{ Form::checkbox("thuworkingday", 1, $category->thuworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='friworkingday'>{{ Form::checkbox("friworkingday", 1, $category->friworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='satworkingday'>{{ Form::checkbox("satworkingday", 1, $category->satworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='sunworkingday'>{{ Form::checkbox("sunworkingday", 1, $category->sunworkingday == 1 ? true : false, ["disabled"]) }}</td>
                                <td field-key='logo'>@if($category->logo)<a href="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}" target="_blank"><img src="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}"/></a>@endif</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('category_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.categories.restore', $category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('category_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.categories.perma_del', $category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('category_view')
                                    <a href="{{ route('admin.categories.show',[$category->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('category_edit')
                                    <a href="{{ route('admin.categories.edit',[$category->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('category_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.categories.destroy', $category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="21">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('category_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.categories.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection