@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.categories.title')</h3>
    
    {!! Form::model($category, ['method' => 'PUT', 'route' => ['admin.categories.update', $category->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('basecode', trans('quickadmin.categories.fields.basecode').'', ['class' => 'control-label']) !!}
                    {!! Form::number('basecode', old('basecode'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('basecode'))
                        <p class="help-block">
                            {{ $errors->first('basecode') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('guid', trans('quickadmin.categories.fields.guid').'', ['class' => 'control-label']) !!}
                    {!! Form::text('guid', old('guid'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('guid'))
                        <p class="help-block">
                            {{ $errors->first('guid') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', trans('quickadmin.categories.fields.name').'', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('mindeliveryordersum', trans('quickadmin.categories.fields.mindeliveryordersum').'', ['class' => 'control-label']) !!}
                    {!! Form::text('mindeliveryordersum', old('mindeliveryordersum'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('mindeliveryordersum'))
                        <p class="help-block">
                            {{ $errors->first('mindeliveryordersum') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('deliverysum', trans('quickadmin.categories.fields.deliverysum').'', ['class' => 'control-label']) !!}
                    {!! Form::text('deliverysum', old('deliverysum'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('deliverysum'))
                        <p class="help-block">
                            {{ $errors->first('deliverysum') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('deliverytime', trans('quickadmin.categories.fields.deliverytime').'', ['class' => 'control-label']) !!}
                    {!! Form::number('deliverytime', old('deliverytime'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('deliverytime'))
                        <p class="help-block">
                            {{ $errors->first('deliverytime') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('opentime', trans('quickadmin.categories.fields.opentime').'', ['class' => 'control-label']) !!}
                    {!! Form::text('opentime', old('opentime'), ['class' => 'form-control timepicker', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('opentime'))
                        <p class="help-block">
                            {{ $errors->first('opentime') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('closetime', trans('quickadmin.categories.fields.closetime').'', ['class' => 'control-label']) !!}
                    {!! Form::text('closetime', old('closetime'), ['class' => 'form-control timepicker', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('closetime'))
                        <p class="help-block">
                            {{ $errors->first('closetime') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('monworkingday', trans('quickadmin.categories.fields.monworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('monworkingday', 0) !!}
                    {!! Form::checkbox('monworkingday', 1, old('monworkingday', old('monworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('monworkingday'))
                        <p class="help-block">
                            {{ $errors->first('monworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tueworkingday', trans('quickadmin.categories.fields.tueworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('tueworkingday', 0) !!}
                    {!! Form::checkbox('tueworkingday', 1, old('tueworkingday', old('tueworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tueworkingday'))
                        <p class="help-block">
                            {{ $errors->first('tueworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('wedworkingday', trans('quickadmin.categories.fields.wedworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('wedworkingday', 0) !!}
                    {!! Form::checkbox('wedworkingday', 1, old('wedworkingday', old('wedworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('wedworkingday'))
                        <p class="help-block">
                            {{ $errors->first('wedworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('thuworkingday', trans('quickadmin.categories.fields.thuworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('thuworkingday', 0) !!}
                    {!! Form::checkbox('thuworkingday', 1, old('thuworkingday', old('thuworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('thuworkingday'))
                        <p class="help-block">
                            {{ $errors->first('thuworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('friworkingday', trans('quickadmin.categories.fields.friworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('friworkingday', 0) !!}
                    {!! Form::checkbox('friworkingday', 1, old('friworkingday', old('friworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('friworkingday'))
                        <p class="help-block">
                            {{ $errors->first('friworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('satworkingday', trans('quickadmin.categories.fields.satworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('satworkingday', 0) !!}
                    {!! Form::checkbox('satworkingday', 1, old('satworkingday', old('satworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('satworkingday'))
                        <p class="help-block">
                            {{ $errors->first('satworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('sunworkingday', trans('quickadmin.categories.fields.sunworkingday').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('sunworkingday', 0) !!}
                    {!! Form::checkbox('sunworkingday', 1, old('sunworkingday', old('sunworkingday')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('sunworkingday'))
                        <p class="help-block">
                            {{ $errors->first('sunworkingday') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    @if ($category->logo)
                        <a href="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}" target="_blank"><img src="{{ env('APP_URL') . '/public/uploads/logos/'.$category->logo }}"></a>
                    @endif
                    {!! Form::label('logo', trans('quickadmin.categories.fields.logo').'', ['class' => 'control-label']) !!}
                    {!! Form::file('logo', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                    {!! Form::hidden('logo_max_size', 15) !!}
                    {!! Form::hidden('logo_max_width', 4096) !!}
                    {!! Form::hidden('logo_max_height', 4096) !!}
                    <p class="help-block"></p>
                    @if($errors->has('logo'))
                        <p class="help-block">
                            {{ $errors->first('logo') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.timepicker').datetimepicker({
                format: "{{ config('app.time_format_moment') }}",
            });
            
        });
    </script>
            
@stop