@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.prices.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.prices.fields.guid')</th>
                            <td field-key='guid'>{{ $price->guid }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.prices.fields.price')</th>
                            <td field-key='price'>{{ $price->price }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.prices.fields.data')</th>
                            <td field-key='data'>{{ $price->data }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.prices.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
