@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orders.title')</h3>
    @can('order_create')
    <p>
        <a href="{{ route('admin.orders.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('order_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.orders.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.orders.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($orders) > 0 ? 'datatable' : '' }} @can('order_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('order_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan
                        <!--
                        <th>@lang('quickadmin.orders.fields.name')</th>
                        -->
                        <th>@lang('quickadmin.orders.fields.phone')</th>
                            <!--
                        <th>@lang('quickadmin.orders.fields.email')</th>
                        -->
                        <th>@lang('quickadmin.orders.fields.address')</th>
                            <!--
                        <th>@lang('quickadmin.orders.fields.people_count')</th>
                        <th>@lang('quickadmin.orders.fields.money_return')</th>
                            <th>@lang('quickadmin.orders.fields.comment')</th>
                            <th>@lang('quickadmin.orders.fields.delivery_datetime')</th>
                            <th>@lang('quickadmin.orders.fields.payment_type')</th>
                            -->
                            <th>@lang('quickadmin.orders.fields.status')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($orders) > 0)
                        @foreach ($orders as $order)
                            <tr data-entry-id="{{ $order->id }}">
                                @can('order_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <!--<td field-key='name'>{{ $order->name }}</td>-->
                                    <td field-key='phone'>{{ $order->phone }}</td>
                                    <!--
                                    <td field-key='email'>{{ $order->email }}</td>
                                    -->
                                    <td field-key='address'>{{ $order->address }}</td>
                                    <!--
                                    <td field-key='people_count'>{{ $order->people_count }}</td>
                                    <td field-key='money_return'>{{ $order->money_return }}</td>
                                    <td field-key='comment'>{{ $order->comment }}</td>
                                    <td field-key='delivery_datetime'>{{ $order->delivery_datetime }}</td>
                                    <td field-key='payment_type'>{{ $order->payment_type }}</td>
                                    -->
                                    <td field-key='status'>{{ $order->status == 1 ? 'Выполнен' : 'Ожидает' }}</td>


                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('order_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orders.restore', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('order_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orders.perma_del', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('order_view')
                                    <a href="{{ route('admin.orders.show',[$order->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('order_edit')
                                    <a href="{{ route('admin.orders.edit',[$order->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('order_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.orders.destroy', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="11">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

@stop

@section('javascript') 
    <script>
        @can('order_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.orders.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection