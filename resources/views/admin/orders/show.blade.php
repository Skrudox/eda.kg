@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <h3 class="page-title">@lang('quickadmin.orders.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <!--
                        <tr>
                            <th>@lang('quickadmin.orders.fields.name')</th>
                            <td field-key='guid'>{{ $order->name }}</td>
                        </tr>
                        -->
                        <tr>
                            <th>@lang('quickadmin.orders.fields.phone')</th>
                            <td field-key='name'>{{ $order->phone }}</td>
                        </tr>
                            <!--
                        <tr>
                            <th>@lang('quickadmin.orders.fields.email')</th>
                            <td field-key='name'>{{ $order->email }}</td>
                        </tr>
                        -->
                        <tr>
                            <th>@lang('quickadmin.orders.fields.address')</th>
                            <td field-key='name'>
                                <div style="margin-bottom: 10px;">{{ $order->address }}</div>
                                <div id='gmap_canvas' style='height:644px;width:100%;'>

                                </div>

                                <style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div>
                <script>

                    function init_map() {
                        var coords = '{{$order->address_coords}}';
                        coords = coords.split(',');
                        //console.log(coords);
                        var myOptions = {
                            zoom: 12,
                            center: new google.maps.LatLng(coords[0], coords[1]),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        // console.log(123);
                        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                        var geocoder = new google.maps.Geocoder;
                        var lastMarker = null;
                        //google.maps.event.addListener(map,'click',function(event) {
                            var str = '';
                            //console.log({lat: coords[0], lng: coords[1]});
                        var myLatlng = new google.maps.LatLng(parseFloat(coords[0]),parseFloat(coords[1]));
                            geocoder.geocode({'location': myLatlng}, function(results, status) {
                                if (status === 'OK') {
                                    if (results[0]) {

                                        str += results[0].formatted_address;
                                        //str += ', ' + event.latLng.lng();
                                        if(lastMarker)
                                        {
                                            lastMarker.setMap(null);
                                        }

                                        $('#address_coords').val(event.latLng.lat() + ',' + event.latLng.lng());
                                        lastMarker = new google.maps.Marker({
                                            position: {lat: event.latLng.lat(), lng: event.latLng.lng()},
                                            map: map,
                                            title: results[0].formatted_address
                                        });

                                        infowindow = new google.maps.InfoWindow({
                                            content: '<strong>' + results[0].formatted_address + '</strong>'
                                        });
                                        infowindow.open(map, lastMarker);
                                        $('.cart input[name="address"]').val(str);
                                    }
                                }
                            });


                        //});
                        //infowindow = new google.maps.InfoWindow({
                        //   content: '<strong>Title</strong><br>London, United Kingdom<br>'
                        // });
                        // google.maps.event.addListener(marker, 'click', function () {
                        //     infowindow.open(map, marker);
                        // });

                    }

                    init_map();

                </script>
                            </td>
                        </tr>
                <!--
                        <tr>
                            <th>@lang('quickadmin.orders.fields.people_count')</th>
                            <td field-key='name'>{{ $order->people_count }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.money_return')</th>
                            <td field-key='name'>{{ $order->money_return }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.comment')</th>
                            <td field-key='name'>{{ $order->comment }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.delivery_datetime')</th>
                            <td field-key='name'>{{ $order->delivery_datetime }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.payment_type')</th>
                            <td field-key='name'>{{ $order->payment_type }}</td>
                        </tr>
                        -->
                        <tr>
                            <th>@lang('quickadmin.orders.fields.data')</th>
                            <td field-key='name'>{!!  $order->data !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orders.fields.status')</th>
                            <td field-key='name'>{{ $order->status == 1 ? 'Выполнен' : 'Ожидает' }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.orders.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
