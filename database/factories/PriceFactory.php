<?php

$factory->define(App\Price::class, function (Faker\Generator $faker) {
    return [
        "guid" => $faker->name,
        "price" => $faker->randomFloat(2, 1, 100),
        "data" => $faker->name,
    ];
});
