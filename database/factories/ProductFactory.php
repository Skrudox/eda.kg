<?php

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        "guid" => $faker->name,
        "name" => $faker->name,
        "isgroup" => 0,
        "isdeleted" => 0,
        "parentguid_id" => factory('App\Category')->create(),
    ];
});
