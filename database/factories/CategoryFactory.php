<?php

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        "basecode" => $faker->randomNumber(2),
        "guid" => $faker->name,
        "name" => $faker->name,
        "mindeliveryordersum" => $faker->randomFloat(2, 1, 100),
        "deliverysum" => $faker->randomFloat(2, 1, 100),
        "deliverytime" => $faker->randomNumber(2),
        "opentime" => $faker->date("H:i:s", $max = 'now'),
        "closetime" => $faker->date("H:i:s", $max = 'now'),
        "monworkingday" => 0,
        "tueworkingday" => 0,
        "wedworkingday" => 0,
        "thuworkingday" => 0,
        "friworkingday" => 0,
        "satworkingday" => 0,
        "sunworkingday" => 0,
    ];
});
