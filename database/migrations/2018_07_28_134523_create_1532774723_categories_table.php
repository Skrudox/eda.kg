<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1532774723CategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('basecode')->nullable();
                $table->string('guid')->unique();
                $table->string('name')->nullable();
                $table->string('url')->unique();
                $table->double('mindeliveryordersum', 15, 2)->nullable();
                $table->double('deliverysum', 15, 2)->nullable();
                $table->integer('deliverytime')->nullable();
                $table->time('opentime')->nullable();
                $table->time('closetime')->nullable();
                $table->tinyInteger('monworkingday')->nullable()->default('0');
                $table->tinyInteger('tueworkingday')->nullable()->default('0');
                $table->tinyInteger('wedworkingday')->nullable()->default('0');
                $table->tinyInteger('thuworkingday')->nullable()->default('0');
                $table->tinyInteger('friworkingday')->nullable()->default('0');
                $table->tinyInteger('satworkingday')->nullable()->default('0');
                $table->tinyInteger('sunworkingday')->nullable()->default('0');
                $table->string('logo');
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
