<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1532781165ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            if(Schema::hasColumn('products', 'guid')) {
                $table->dropColumn('guid');
            }
            if(Schema::hasColumn('products', 'parentguid')) {
                $table->dropColumn('parentguid');
            }
            
        });
Schema::table('products', function (Blueprint $table) {
            
if (!Schema::hasColumn('products', 'guid')) {
                $table->string('guid')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('guid');
            
        });
Schema::table('products', function (Blueprint $table) {
                        $table->string('guid')->nullable();
                $table->string('parentguid')->nullable();
                
        });

    }
}
