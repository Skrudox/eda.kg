<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1532860327CategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            if(Schema::hasColumn('categories', 'wedworkingday')) {
                $table->dropColumn('wedworkingday');
            }
            if(Schema::hasColumn('categories', 'thuworkingday')) {
                $table->dropColumn('thuworkingday');
            }
            if(Schema::hasColumn('categories', 'friworkingday')) {
                $table->dropColumn('friworkingday');
            }
            if(Schema::hasColumn('categories', 'satworkingday')) {
                $table->dropColumn('satworkingday');
            }
            if(Schema::hasColumn('categories', 'sunworkingday')) {
                $table->dropColumn('sunworkingday');
            }
            
        });
Schema::table('categories', function (Blueprint $table) {
            
if (!Schema::hasColumn('categories', 'wedworkingday')) {
                $table->tinyInteger('wedworkingday')->nullable()->default('0');
                }
if (!Schema::hasColumn('categories', 'thuworkingday')) {
                $table->tinyInteger('thuworkingday')->nullable()->default('0');
                }
if (!Schema::hasColumn('categories', 'friworkingday')) {
                $table->tinyInteger('friworkingday')->nullable()->default('0');
                }
if (!Schema::hasColumn('categories', 'satworkingday')) {
                $table->tinyInteger('satworkingday')->nullable()->default('0');
                }
if (!Schema::hasColumn('categories', 'sunworkingday')) {
                $table->tinyInteger('sunworkingday')->nullable()->default('0');
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('wedworkingday');
            $table->dropColumn('thuworkingday');
            $table->dropColumn('friworkingday');
            $table->dropColumn('satworkingday');
            $table->dropColumn('sunworkingday');
            
        });
Schema::table('categories', function (Blueprint $table) {
                        $table->string('wedworkingday')->nullable();
                $table->string('thuworkingday')->nullable();
                $table->string('friworkingday')->nullable();
                $table->string('satworkingday')->nullable();
                $table->string('sunworkingday')->nullable();
                
        });

    }
}
