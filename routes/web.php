<?php
//Route::get('/', function () { return redirect('/admin/home'); });
Route::get('/admin', 'Admin\UsersController@index')->middleware('auth');
Route::get('/', 'HomeController@index')->name('/');
Route::get('/search', 'HomeController@search');//->middleware('auth');
Route::get('/restaurants', 'HomeController@categories');
Route::get('/kitchens', 'HomeController@categories');
Route::get('ajax_cats', 'HomeController@categories_ajax');
Route::get('/popular_orders', 'HomeController@popular_orders');
Route::get('/popluar_orders/get/{id}', 'HomeController@popular_orders_get');
Route::get('/category/{url}', 'HomeController@category_view');
Route::get('/category/{url}/{sub}', 'HomeController@category_view');
Route::get('/page/{page}', 'HomeController@page');
$this->get('cart-data', 'HomeController@cart_data');
$this->post('add-to-cart', 'HomeController@add_to_cart');
$this->get('clear-cart', 'HomeController@clear_cart');
$this->post('cart-plus-item', 'HomeController@cart_plus_item');
$this->post('cart-minus-item', 'HomeController@cart_minus_item');
$this->post('cart-delete-item', 'HomeController@cart_delete_item');
$this->get('checkout-cart', 'HomeController@checkout_cart');
$this->post('cart-submit', 'HomeController@submit_cart');
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('products', 'Admin\ProductsController');
    Route::post('products_mass_destroy', ['uses' => 'Admin\ProductsController@massDestroy', 'as' => 'products.mass_destroy']);
    Route::post('products_restore/{id}', ['uses' => 'Admin\ProductsController@restore', 'as' => 'products.restore']);
    Route::delete('products_perma_del/{id}', ['uses' => 'Admin\ProductsController@perma_del', 'as' => 'products.perma_del']);
    Route::resource('categories', 'Admin\CategoriesController');
    Route::post('categories_mass_destroy', ['uses' => 'Admin\CategoriesController@massDestroy', 'as' => 'categories.mass_destroy']);
    Route::post('categories_restore/{id}', ['uses' => 'Admin\CategoriesController@restore', 'as' => 'categories.restore']);
    Route::delete('categories_perma_del/{id}', ['uses' => 'Admin\CategoriesController@perma_del', 'as' => 'categories.perma_del']);
    Route::resource('prices', 'Admin\PricesController');
    Route::post('prices_mass_destroy', ['uses' => 'Admin\PricesController@massDestroy', 'as' => 'prices.mass_destroy']);
    Route::post('prices_restore/{id}', ['uses' => 'Admin\PricesController@restore', 'as' => 'prices.restore']);
    Route::delete('prices_perma_del/{id}', ['uses' => 'Admin\PricesController@perma_del', 'as' => 'prices.perma_del']);
    Route::post('/spatie/media/upload', 'Admin\SpatieMediaController@create')->name('media.upload');
    Route::post('/spatie/media/remove', 'Admin\SpatieMediaController@destroy')->name('media.remove');

    Route::resource('orders', 'Admin\OrdersController');
    Route::post('orders_mass_destroy', ['uses' => 'Admin\OrdersController@massDestroy', 'as' => 'orders.mass_destroy']);
    Route::post('orders_restore/{id}', ['uses' => 'Admin\OrdersController@restore', 'as' => 'orders.restore']);

    Route::resource('pages', 'Admin\PagesController');
    Route::post('pages_mass_destroy', ['uses' => 'Admin\PagesController@massDestroy', 'as' => 'pages.mass_destroy']);
    Route::post('pages_restore/{id}', ['uses' => 'Admin\PagesController@restore', 'as' => 'pages.restore']);
 
});
